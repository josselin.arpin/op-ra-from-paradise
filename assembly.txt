Pour le montage :
Dans des rectangles inox individuels, disposer 6 rectangles de biscuit, puis les imbiber de sirop au café à l’aide d’un pinceau.
Sur le premier, verser de la ganache, poser dessus un rectangle de biscuit imbibé de sirop, puis verser de la crème au beurre. Déposer un rectangle de biscuit et verser à nouveau de la ganache. Poser ensuite le dernier rectangle de biscuit puis réserver le gâteau au frais.
Pour finir, couvrir le dernier biscuit d’une fine couche de glaçage au chocolat.
Réserver au frais pendant 1 heure, retirer les rectangles inox et servir.

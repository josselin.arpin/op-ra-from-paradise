1. Laisser le beurre à température ambiante pour obtenir un beurre pommade.
2. Réaliser l’équivalent d’un café serré.
3. Réaliser un sirop en faisant cuire le sucre et l’eau pour obtenir un sucre “petit boulé” (120 °C).
4. Dans le bol du batteur, monter les oeufs entiers et les jaunes pour obtenir une mousse blanche, puis verser le sirop dessus tout en continuant de monter les oeufs. Fouetter jusqu’à ce que le mélange refroidisse. Lorsqu’il est froid, incorporer le beurre pommade, le café et réserver la crème au beurre.